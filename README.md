<div align="center">
  <h1>Neptune Engine</h1>
  <img src="../resources/icon.png" width="192px" /><hr/>
</div>

<div align="left">
  <img alt="Latest Release" src="https://img.shields.io/github/v/release/FrioGitHub/NeptuneEngine?color=green&label=Latest%20Release&style=for-the-badge"><br>
  <img alt="Vulnerabilities" src="https://img.shields.io/snyk/vulnerabilities/github/FrioGitHub/NeptuneEngine?color=green&style=for-the-badge"><br>
  <img alt="CodeFactor Grade" src="https://img.shields.io/codefactor/grade/github/FrioGitHub/NeptuneEngine/master?color=green&style=for-the-badge"><br>
  <img alt="Downloads" src="https://img.shields.io/github/downloads/FrioGitHub/NeptuneEngine/total?color=green&style=for-the-badge"><br>
  <img alt="Repository size" src="https://img.shields.io/github/repo-size/FrioGitHub/NeptuneEngine?color=green&style=for-the-badge"><br>
  <img alt="License" src="https://img.shields.io/github/license/FrioGitHub/NeptuneEngine?color=green&style=for-the-badge"><br>
  <img alt="Workflow Status" src="https://img.shields.io/github/workflow/status/FrioGitHub/NeptuneEngine/Android%20CI/master?color=green&style=for-the-badge">
</div>

<hr />

<b>Neptune Engine is an simple game engine written in Java for Android devices using OpenGL ES</b>

<hr />

<h1>Screenshots</h1>

<div align="center">
  <img src="../resources/demo1.png" width="100%" />
  <img src="../resources/demo2.png" width="100%" />
</div>

<h1>Warning</h1>

> **Warning**:
> This game engine is currently in development!
